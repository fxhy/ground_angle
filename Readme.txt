Dieses Package wurde unter folgenden Vorraussetzungen erstellt:

Ubuntu:
    Distributor ID:	Ubuntu
    Description:	Ubuntu 16.04.4 LTS
    Release:	16.04
    Codename:	xenial
ROS:
    Kinetic Kame

    Installationsanleitung:
        http://wiki.ros.org/kinetic/Installation/Ubuntu
    build system:
        catkin
rviz:
    http://wiki.ros.org/rviz/UserGuide
rqt_image_view:
    http://wiki.ros.org/rqt_image_view
Testdaten:
    https://gitlab.com/fxhy/ground_angle.git
---------------------------------------------------------------
1. Workspace erstellen
    http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2. ground_angle package in den src Ordner kopieren
    https://gitlab.com/fxhy/ground_angle.git

3. PCL for ROS_Kinetic in den src Ordner kopieren
    https://github.com/ros-perception/perception_pcl/tree/kinetic-devel

4. $ catkin_make install
5. Terminal 1: $ roscore
6. Terminal 2: $ source devel/setup.bash
7. Terminal 2: $ rosrun ground_angle ground_angle input:=/velodyne_points
8. Terminal 3: $ rosbag play .../ground_angle/TestData/-...-.bag
9. Terminal 4: $ rviz rviz 
                        Fixed Frame: velodyne
                            ->Add ->By topic ->velodyne_points ->PointCloud2
10.Terminal 5: $ rqt_image_view 
                        oben links im Dropdown "/camera/ImageTransport"