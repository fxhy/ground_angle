#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <iomanip>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <vector>
#include <numeric>
#include <ground_angle/Deg.h>

#define PI 3.14159265

ros::Publisher pub;
ros::Publisher pub1;
ros::Publisher pub2;

void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
  ground_angle::Deg msg;
  msg.timestamp = cloud_msg->header.stamp;
  // Container for original & filtered data
  ros::Time begin = ros::Time::now();
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(*cloud_msg,*cloud);
  pcl::PointCloud<pcl::PointXYZ> cloud_c;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_cb (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

   // Perform CropBox filtering
   // Chapter: 5.1.1
  pcl::CropBox<pcl::PointXYZ> boxFilter;
  //Min:(1, -0.5, -0.4, 1.0)
  boxFilter.setMin(Eigen::Vector4f(0.1, -0.5, -1, 1.0));
  //Max:(15, 0.5, 0.4, 1.0)
  boxFilter.setMax(Eigen::Vector4f(10, 0.5, 0.5, 1.0));
  boxFilter.setInputCloud(cloud);
  boxFilter.filter(cloud_c);
  *cloud_filtered_cb = cloud_c;
  pub1.publish(cloud_c);
  
  // Create the filtering object: downsample the dataset using a leaf size of 1cm
  // Chapter: 5.1.2
  pcl::VoxelGrid<pcl::PointXYZ> vg;
  vg.setInputCloud (cloud_filtered_cb);
  vg.setLeafSize (0.01, 0.01, 0.01);
  vg.filter (*cloud_filtered);
  pub2.publish(cloud_filtered);

  //Check if PointCloud contains points
  if (cloud_filtered->points.size () >3)
  {
  
  // Find the planar coefficients for floor plane
  // Chapter: 5.2
      pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
      pcl::PointIndices::Ptr floor_inliers (new pcl::PointIndices);
      pcl::SACSegmentation<pcl::PointXYZ> seg;
      seg.setOptimizeCoefficients (true);
      // Chapter: 5.2.1
      seg.setModelType (pcl::SACMODEL_PLANE);
      // Chapter: 5.2.2
      seg.setMethodType (pcl::SAC_RANSAC);
      seg.setDistanceThreshold (0.01);
      seg.setInputCloud (cloud_filtered);
      seg.segment (*floor_inliers, *coefficients);
      
      

      // Chapter: 5.3
      Eigen::Vector3f v_coeff(coefficients->values[0], coefficients->values[1], coefficients->values[2]);
      Eigen::Vector3f v_norm(0,0,1);
      float p_coeff = sqrt(pow(v_coeff[0],2) + pow(v_coeff[1],2) + pow(v_coeff[2],2));
      float co_al;
      float v_result = v_coeff[0] * v_norm[0] + v_coeff[1] * v_norm[1] + v_coeff[2] * v_norm[2];

      co_al = v_result/p_coeff;

      float theta = acos (co_al) * 180.0/PI;      

      ros::Duration dura = ros::Time::now() - begin ;

      //std::cout << dura << std::endl;
      
      
      msg.deg=theta;
      std::cout << "Rotation Angle: "<< msg.deg << std::endl;

  
  // Publish the data.
  pub.publish (msg);
  }
  else
  {
    std::cout << "PointCloud ist leer! " << std::endl;
  }
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "ground_angle");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("input", 1, cloud_cb);

  // Create a ROS publisher for the output Angle
  pub = nh.advertise<ground_angle::Deg> ("PitchAngle", 1);
  pub1 = nh.advertise<sensor_msgs::PointCloud2> ("cb", 1);
  pub2 = nh.advertise<sensor_msgs::PointCloud2> ("vg", 1);

  // Spin
  ros::spin ();
}
